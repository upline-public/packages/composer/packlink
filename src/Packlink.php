<?php

namespace Uplinestudio\Packlink;


use Error;
use Uplinestudio\Packlink\Data\AuthData;
use Uplinestudio\Packlink\Data\DraftRequestData;
use Uplinestudio\Packlink\Data\TrackingData;
use Uplinestudio\Packlink\Utility\Request;
use Uplinestudio\Packlink\v1\Draft;

class Packlink
{
    private AuthData $authData;

    private const SHIPMENT_URL = '/shipments';
    private const LABELS_URL = '/labels';

    public function __construct(AuthData $authData)
    {
        $this->authData = $authData;
    }

    public function createDraft(DraftRequestData $requestData): Draft
    {
        $request = new Request($this->authData, $requestData->getRequestData());

        $response = $request->send(self::SHIPMENT_URL);

        $responseData = $response->getResponseData();

        $errorMessage = $response->getErrorMessage();

        if ($errorMessage) {
            throw new Error($errorMessage);
        }

        if (!isset($responseData['reference'])) {
            throw new Error('No Draft Reference Code Found In Response');
        }

        return new Draft($responseData['reference']);
    }

    public function getLabelUrls(Draft $draft): array
    {
        $request = new Request($this->authData);

        $url = self::SHIPMENT_URL . '/' . $draft->getReference() . self::LABELS_URL;

        $response = $request->send($url);

        $responseData = $response->getResponseData();

        $errorMessage = $response->getErrorMessage();;

        if ($errorMessage) {
            throw new Error($errorMessage);
        }

        if (!is_array($responseData) || count($responseData) === 0) {
            throw new Error('No Labels Found In Response');
        }

        return $responseData;
    }

    public function getShipmentTrackingData(Draft $draft) :?TrackingData
    {
        $request = new Request($this->authData);
        $url = self::SHIPMENT_URL . '/' . $draft->getReference();
        $response = $request->send($url);

        if ($response->getErrorMessage()) {
            return null;
        }

        $responseData = $response->getResponseData();

        if ($responseData) {
            return new TrackingData($responseData['service'] ?? null, $responseData['carrier'] ?? null, $responseData['trackings'] ?? null, $responseData['tracking_url'] ?? null);
        }

        return null;
    }
}
