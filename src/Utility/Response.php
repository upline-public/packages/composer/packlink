<?php

namespace Uplinestudio\Packlink\Utility;

class Response
{

    private ?array $responseData;

    private ?string $errorMessage = null;

    public function __construct(string $response)
    {
        $this->responseData = json_decode($response, true);

        if (!$this->responseData) {
            $this->errorMessage = 'Empty Response';
            return;
        }

        if (isset($this->responseData['message'])) {
            $this->errorMessage = $this->responseData['message'];
            return;
        }

        if (isset($this->responseData['messages']) && isset($this->responseData['messages']['message'])) {
            $this->errorMessage = $this->responseData['messages']['message'];
        }
    }

    public function getResponseData(): ?array
    {
        return $this->responseData;
    }

    public function getErrorMessage(): ?string
    {
        return $this->errorMessage;
    }
}
