<?php

namespace Uplinestudio\Packlink\Utility;

use Uplinestudio\Packlink\Data\CallbackData;

class CallbackService
{
    private ?string $originalRequest;

    private ?CallbackData $callbackData;

    public function __construct(string $request)
    {
        if (!$request) {
            return;
        }

        $this->originalRequest = $request;

        $requestArray = json_decode($request, true);

        if (
            isset($requestArray['event'])
            && isset($requestArray['datetime'])
            && isset($requestArray['data'])
            && isset($requestArray['data']['shipment_reference'])
        ) {
            $shipmentCustomReference = $requestArray['data']['shipment_custom_reference'] ?? '';
            $this->callbackData = new CallbackData(
                $requestArray['event'],
                $requestArray['datetime'],
                $requestArray['data']['shipment_reference'],
                $shipmentCustomReference
            );
        }
    }

    /**
     * @return CallbackData|null
     */
    public function getCallbackData(): ?CallbackData
    {
        return $this->callbackData;
    }

    /**
     * @return string|null
     */
    public function getOriginalRequest(): ?string
    {
        return $this->originalRequest;
    }
}
