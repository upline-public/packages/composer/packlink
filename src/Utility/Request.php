<?php

namespace Uplinestudio\Packlink\Utility;

use Uplinestudio\Packlink\Data\AuthData;
use Uplinestudio\Packlink\Data\RequestData;

class Request
{
    private AuthData $authData;
    private ?RequestData $requestData;

    public function __construct(AuthData $authData, ?RequestData $requestData = null)
    {
        $this->authData = $authData;
        $this->requestData = $requestData;
    }

    public function send($url): Response
    {
        $ch = curl_init($this->authData->getUrl() . $url);

        $options = [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_HTTPHEADER => [
                "Authorization:" . $this->authData->getToken(),
                "Content-Type:application/json",
            ],
        ];

        if ($this->requestData) {
            $options[CURLOPT_CUSTOMREQUEST] = "POST";
            $options[CURLOPT_POSTFIELDS] = $this->requestData->json();
        }

        curl_setopt_array($ch, $options);

        $response = curl_exec($ch);

        curl_close($ch);

        return new Response($response);
    }
}
