<?php

namespace Uplinestudio\Packlink\Data;

class TrackingData
{
    private ?string $service;
    private ?string $carrier;
    private ?array $trackings;
    private ?string $trackingUrl;

    public function __construct(?string $service, ?string $carrier, ?array $trackings, ?string $trackingUrl)
    {

        $this->service = $service;
        $this->carrier = $carrier;
        $this->trackings = $trackings;
        $this->trackingUrl = $trackingUrl;
    }

    /**
     * @return string|null
     */
    public function getService(): ?string
    {
        return $this->service;
    }

    /**
     * @return string|null
     */
    public function getCarrier(): ?string
    {
        return $this->carrier;
    }

    /**
     * @return array|null
     */
    public function getTrackings(): ?array
    {
        return $this->trackings;
    }

    /**
     * @return string|null
     */
    public function getTrackingUrl(): ?string
    {
        return $this->trackingUrl;
    }
}
