<?php

namespace Uplinestudio\Packlink\Data;

class RequestData
{
    /**
     * @var mixed
     */
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @return false|string
     */
    public function json()
    {
        return json_encode($this->data);
    }
}
