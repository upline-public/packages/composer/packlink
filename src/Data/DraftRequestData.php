<?php

namespace Uplinestudio\Packlink\Data;


class DraftRequestData
{
    private PersonData $from;
    private PersonData $to;
    private string $content;
    private float $contentValue;
    /**
     * @var PackageData[]
     */
    private array $packages;

    /**
     * @param PersonData $from
     * @param PersonData $to
     * @param string $content
     * @param float $contentValue
     * @param PackageData[] $packages
     */
    public function __construct(PersonData $from, PersonData $to, string $content, float $contentValue, array $packages)
    {
        $this->from = $from;
        $this->to = $to;
        $this->content = $content;
        $this->contentValue = $contentValue;
        $this->packages = $packages;
    }

    private function getPackages(): array
    {
        $packages = [];
        foreach ($this->packages as $package) {
            $packages[] = [
                'length' => $package->getLength(),
                'width' => $package->getWidth(),
                'height' => $package->getHeight(),
                'weight' => $package->getWeight()
            ];
        }
        return $packages;
    }

    public function getRequestData(): RequestData
    {
        return new RequestData([
            'from' => [
                'street1' => $this->from->getStreetWithHouse(),
                'street2' => $this->from->getStreet2(),
                'city' => $this->from->getCity(),
                'state' => $this->from->getState(),
                'country' => $this->from->getCountry(),
                'phone' => $this->from->getPhone(),
                'email' => $this->from->getEmail(),
                'name' => $this->from->getName(),
                'surname' => $this->from->getSurname(),
                'company' => $this->from->getCompany(),
                'zip_code' => $this->from->getZip()
            ],
            'to' => [
                'street1' => $this->to->getStreetWithHouse(),
                'street2' => $this->to->getStreet2(),
                'city' => $this->to->getCity(),
                'state' => $this->to->getState(),
                'country' => $this->to->getCountry(),
                'phone' => $this->to->getPhone(),
                'email' => $this->to->getEmail(),
                'name' => $this->to->getName(),
                'surname' => $this->to->getSurname(),
                'zip_code' => $this->to->getZip()
            ],
            'content' => $this->content,
            'contentvalue' => ceil($this->contentValue * 100) / 100,
            'packages' => $this->getPackages()
        ]);
    }
}
