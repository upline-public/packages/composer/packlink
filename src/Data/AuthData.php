<?php

namespace Uplinestudio\Packlink\Data;

class AuthData
{
    /**
     * @var string
     */
    private string $url;
    /**
     * @var string
     */
    private string $token;

    public function __construct(string $url, string $token)
    {
        $this->url = $url;
        $this->token = $token;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }
}
