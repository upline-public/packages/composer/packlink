<?php

namespace Uplinestudio\Packlink\Data;

class CallbackData
{
    private string $event;
    private string $dateTime;
    private string $shipmentReference;
    private string $shipmentCustomReference;

    public function __construct(
        string $event,
        string $dateTime,
        string $shipmentReference,
        string $shipmentCustomReference = ""
    )
    {

        $this->event = $event;
        $this->dateTime = $dateTime;
        $this->shipmentReference = $shipmentReference;
        $this->shipmentCustomReference = $shipmentCustomReference;
    }

    /**
     * @return string
     */
    public function getEvent(): string
    {
        return $this->event;
    }

    /**
     * @return string
     */
    public function getDateTime(): string
    {
        return $this->dateTime;
    }

    /**
     * @return string
     */
    public function getShipmentReference(): string
    {
        return $this->shipmentReference;
    }

    /**
     * @return string
     */
    public function getShipmentCustomReference(): string
    {
        return $this->shipmentCustomReference;
    }
}
