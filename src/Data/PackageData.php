<?php

namespace Uplinestudio\Packlink\Data;

class PackageData
{
    private float $width;
    private float $height;
    private float $length;
    private float $weight;

    /**
     * @param float $width cm
     * @param float $height cm
     * @param float $length cm
     * @param float $weight kg
     */
    public function __construct(
        float $width,
        float $height,
        float $length,
        float $weight
    )
    {

        $this->width = $width;
        $this->height = $height;
        $this->length = $length;
        $this->weight = $weight;
    }

    private static function convertToPrecision(float $number)
    {
        return ceil($number * 100) / 100;
    }

    /**
     * @return float
     */
    public function getWidth(): float
    {
        return self::convertToPrecision($this->width);
    }

    /**
     * @return float
     */
    public function getHeight(): float
    {
        return self::convertToPrecision($this->height);
    }

    /**
     * @return float
     */
    public function getLength(): float
    {
        return self::convertToPrecision($this->length);
    }

    /**
     * @return float
     */
    public function getWeight(): float
    {
        return self::convertToPrecision($this->weight);
    }

}
