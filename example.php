<?php

use Uplinestudio\Packlink\Data\AuthData;
use Uplinestudio\Packlink\Data\DraftRequestData;
use Uplinestudio\Packlink\Data\PackageData;
use Uplinestudio\Packlink\Data\PersonData;
use Uplinestudio\Packlink\Packlink;
use Uplinestudio\Packlink\Utility\CallbackService;
use Uplinestudio\Packlink\v1\Draft;

require_once __DIR__ . '/vendor' . DIRECTORY_SEPARATOR . 'autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$authData = new AuthData($_ENV['URL'], $_ENV['TOKEN']);

$packlink = new Packlink($authData);

$senderData = [
    'name' => 'Name',
    'surname' => 'Surname',
    'email' => 'test@email.com',
    'phone' => '1234567890',
    'country' => 'DE',
    'city' => 'City',
    'street' => 'Street',
    'house' => '1',
    'zip' => '12345',
    'company' => 'Company Name',
    'state' => 'State'
    ];

$personFrom = new PersonData(
    $senderData['name'],
    $senderData['surname'],
    $senderData['email'],
    $senderData['phone'],
    $senderData['country'],
    $senderData['city'],
    $senderData['street'],
    $senderData['house'],
    $senderData['zip']
);

$personFrom->setCompany($senderData['company']);

$personFrom->setState($senderData['state']);

$personTo = new PersonData(
    'Test Name',
    'Test Surname',
    'test@test.com',
    '0987654321',
    'DE',
    'Test City',
    'Test Street',
    '9',
    '98765'
);

$personTo->setStreet2('optional street');

$content = 'Test Contents';

$contentValue = 150.334;

$package = new PackageData(20, 20.222, 20, 1);

$requestData = new DraftRequestData($personFrom, $personTo, $content, $contentValue, [$package]);

$draft = $packlink->createDraft($requestData);

$labels = $packlink->getLabelUrls($draft);

var_dump($labels);

$trackingData = $packlink->getShipmentTrackingData($draft);

var_dump($trackingData);
